import { Component, OnInit } from '@angular/core';
import { QuestionsService } from '../../core/services/questions.service';
import { ActivatedRoute } from '@angular/router';
import { combineLatest, Subscription } from 'rxjs';
import { switchMap, tap, skipWhile } from 'rxjs/operators';
import { QuestionModel } from '../../core/state/question.model';

@Component({
  selector: 'app-play',
  templateUrl: './play.component.html',
  styleUrls: ['./play.component.scss']
})
export class PlayComponent implements OnInit {
  questions$ = this.questionsService.questions$;
  gettingQuestions$ = this.questionsService.gettingQuestions$;
  gameCompleted$ = this.questionsService.gameCompleted$;
  gameScore = 0;
  showScore = false;

  getQuestionsSubscription: Subscription = this.route.queryParams
    .pipe(switchMap(params =>
      this.questionsService.getQuestions({
        type: params.type,
        amount: params.amount,
        difficulty: params.difficulty
      })
    )).subscribe();


  constructor(
    private readonly route: ActivatedRoute,
    private readonly questionsService: QuestionsService,
  ) { 
    combineLatest(([this.questions$, this.gameCompleted$]))
    .subscribe(([questions, gameCompleted]) => {
      if(gameCompleted == true)
      {
        console.log('Game completed ');
        let countScore = 0;
        questions.forEach(question => {
          const correct_answer = question.answers.find(answer => answer.isCorrect)?._id;
          if(question.selectedId == correct_answer)
            countScore++;
        });
        this.gameScore = countScore;
        this.showScore = true;
      }
    })

  }

  ngOnInit(): void {
    
  
  }

  onAnswerClicked(questionId: QuestionModel['_id'], answerSelected: string): void {
    this.questionsService.selectAnswer(questionId, answerSelected);
  
  }

}
